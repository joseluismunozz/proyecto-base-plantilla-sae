import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotAuthGuard } from '../guards/not-auth.guard';
import { HomeComponent } from '../pages/home/home.component';
import { PagesRoutingModule } from '../pages/pages-routing.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'home', component: HomeComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'forgot', component: ForgotPasswordComponent },
      { path: '**', redirectTo: 'home', pathMatch: 'full' }

     
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),PagesRoutingModule],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
