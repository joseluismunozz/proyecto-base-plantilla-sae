import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  title = 'plantilla_sae';
  names = localStorage.getItem('names');
  surnames = localStorage.getItem('surnames');
  role = localStorage.getItem('role');

  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  
  constructor(changeDetectorRef: ChangeDetectorRef,  media: MediaMatcher, ) {

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

   }

  ngOnInit(): void {
  }
   opener(event:any){

    let elements = document.querySelectorAll('.active');
    
    if( event.srcElement.classList.length==1){
      event.srcElement.classList.add('active');
      if(elements.length>0){
        elements.forEach(function(item) {
          item.classList.remove('active')
        });  
           
      };
     
    }else{
      let openers = document.querySelectorAll('.active');
      openers.forEach(function(item) {
        item.classList.remove('active')
         
      });
    }
     
  }
 

}
