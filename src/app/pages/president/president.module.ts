import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PresidentRoutingModule } from './president-routing.module';
import { AddNewComponent } from './add-new/add-new.component';
import { EditNewComponent } from './edit-new/edit-new.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { TableFilterComponent } from './table-filter/table-filter.component';
import { SharedModule } from '../../shared/shared.module';
import { CustomerService } from './table-filter/customerservice';
import { DropdownModule } from 'primeng/dropdown';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  declarations: [
    AddNewComponent,
    EditNewComponent,
    TableFilterComponent

  ],
  imports: [
    BrowserModule,
    CommonModule,
    PresidentRoutingModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    DropdownModule
  ],
  providers: [CustomerService],

})
export class PresidentModule { }
