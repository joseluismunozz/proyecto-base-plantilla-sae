import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { AllMembersComponent } from './president/all-members/all-members.component';
import { AllProjectsComponent } from './president/all-projects/all-projects.component';
import { ProjectRequestComponent } from './president/project-request/project-request.component';
import { RegisterRequestComponent } from './president/register-request/register-request.component';
import { HomeComponent } from './home/home.component';
import { PagesComponent } from './pages.component';

import { SharedModule } from '../shared/shared.module';
import { PresidentModule } from './president/president.module';


@NgModule({
  declarations: [
    AllMembersComponent,
    AllProjectsComponent,
    ProjectRequestComponent,
    RegisterRequestComponent,
    HomeComponent,
    PagesComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule,
    PresidentModule

  ],

})
export class PagesModule { }
