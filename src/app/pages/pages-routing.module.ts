import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllMembersComponent } from './president/all-members/all-members.component';
import { AllProjectsComponent } from './president/all-projects/all-projects.component';
import { AllProjectsComponent as AllProjectComponentEditor } from './editor/all-projects/all-projects.component';
import { AllProjectsComponent as AllProjectComponentInvestigator } from './investigator/all-projects/all-projects.component';
import { AllProjectsComponent as AllProjectsComponentReviewer } from './reviewer/all-projects/all-projects.component';
import { ProjectRequestComponent } from './president/project-request/project-request.component';
import { ProjectRequestComponent as ProjectRequestComponentEditor} from './editor/project-request/project-request.component';
import { RegisterRequestComponent } from './president/register-request/register-request.component';
import { PendingprojectsComponent } from './editor/pendingprojects/pendingprojects.component';
import { AddNewComponent } from './president/add-new/add-new.component';
import { EditNewComponent } from './president/edit-new/edit-new.component';
import { AddProjectRequestComponent } from './investigator/add-project-request/add-project-request.component';
import { PendingInvestigationsComponent } from './reviewer/pending-investigations/pending-investigations.component';
import { PendingDocumentsComponent } from './secretary/pending-documents/pending-documents.component';
import { HomeComponent } from './home/home.component';
import { PagesComponent } from './pages.component';
import { TableFilterComponent } from './president/table-filter/table-filter.component';

const routes: Routes = [
  

  {
    path:'admin',
    component:PagesComponent,
    children : [
      {
        path : 'allmembers',
        component : AllMembersComponent,
        data: { breadcrumb: 'Todos los miembros' , title : 'Todos los miembros'}
      },
      {
        path : 'memberrequest',
        component : RegisterRequestComponent,
        data: { breadcrumb: 'Solicitud de registros' , title : 'Solicitudes de registro'}
      },
      {
        path : 'allprojects',
        component : AllProjectsComponent
      },
      {
        path: 'projestrequest',
        component : ProjectRequestComponent
      },
      {
        path: 'addnew',
        component : AddNewComponent
      },
      {
        path: 'editnew',
        component : EditNewComponent
      },
      {
        path: 'table',
        component : TableFilterComponent
      },
    
    ]
    
  },
  {
    path: 'editor',
    component:PagesComponent,
    children : [
      {
        path: 'projestrequest',
        component : ProjectRequestComponentEditor
      },
      {
        path: 'allprojects',
        component : AllProjectComponentEditor 
      },
      {
        path : 'pendingprojects',
        component : PendingprojectsComponent
      }
      
    ]
  },
  {
    path : 'investigator',
    component:PagesComponent,

    children : [
      {
        path: 'addrequest',
        component : AddProjectRequestComponent
      },
      {
        path: 'allprojects',
        component : AllProjectComponentInvestigator
      },
    ]
  },
  {
    path : 'reviewer',
    component:PagesComponent,

    children : [
      {
        path: 'proyectos',
        component : AllProjectsComponentReviewer
      },
      {
        path: 'investigacionespendientes',
        component : PendingInvestigationsComponent
      },
    ]
  },
  {
    path : 'secretary',
    component:PagesComponent,

    children : [
      {
        path: 'documentospendientes',
        component : PendingDocumentsComponent
      }
    ]
  },
  

];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
