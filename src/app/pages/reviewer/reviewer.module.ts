import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewerRoutingModule } from './reviewer-routing.module';
import { PendingInvestigationsComponent } from './pending-investigations/pending-investigations.component';
import { AllProjectsComponent } from './all-projects/all-projects.component';


@NgModule({
  declarations: [
    PendingInvestigationsComponent,
    AllProjectsComponent
  ],
  imports: [
    CommonModule,
    ReviewerRoutingModule
  ]
})
export class ReviewerModule { }
