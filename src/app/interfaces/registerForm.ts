export interface registerForm {
    names:string,
    surnames:string,
    document_type :string,
    document:string,
    email:string,
    phone:string,
    password:string,
    password_confirm:string,
}